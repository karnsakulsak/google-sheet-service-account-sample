<?php

/**
 * Use JWT encoding script from Michel via 
 * https://stackoverflow.com/questions/48106432/invalid-signature-google-jwt-api
 */

// Load credential file
$credential = json_decode(file_get_contents('credential.json'));

// Build JWT
$jwtHeader = base64url_encode(json_encode(array(
    'alg' => 'RS256',
    'typ' => 'JWT'
)));
$now = time();
$jwtClaim = base64url_encode(json_encode(array(
    'iss' => $credential->client_email,
    'scope' => 'https://www.googleapis.com/auth/spreadsheets',
    'aud' => 'https://www.googleapis.com/oauth2/v4/token',
    'iat' => $now, 
    'exp' => $now + 3600,
)));
$binarySignature = '';
$algo = 'SHA256';
openssl_sign("$jwtHeader.$jwtClaim", $binarySignature, $credential->private_key, $algo);
$jwtSign = base64url_encode($binarySignature);
$jwtAssertion = "$jwtHeader.$jwtClaim.$jwtSign";
echo "JWT Assertion:\n", $jwtAssertion, "\n";

// Request access token by send JWT
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://www.googleapis.com/oauth2/v4/token");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
    'grant_type' => 'urn:ietf:params:oauth:grant-type:jwt-bearer',
    'assertion' => $jwtAssertion,
)));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$result = curl_exec($ch);
$token = json_decode($result);
echo $result, "\n";
curl_close($ch);

if (!property_exists($token, 'access_token')) {
    die('ERROR');
}

// Test write to privately shared Google Sheets
$sheetInfo = json_decode(file_get_contents('sheet-info.json'));
$tag = '123';
$valueInputOption = 'USER_ENTERED'; // RAW | USER_ENTERED
$data = array(
    'range' => 'B2:C4',
    'majorDimension' => 'ROWS',
    'values' => array(
        array('B2-' . $tag, 'C2-' . $tag),
        array('B3', 'C3'),
        array('1234', '5678'),
    )
);
$dataJSON = json_encode($data);
$ch = curl_init();
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    'Content-Length: ' . strlen($dataJSON),
    'Authorization: Bearer ' . $token->access_token
));
curl_setopt($ch, CURLOPT_URL, 
    "https://sheets.googleapis.com/v4/spreadsheets/{$sheetInfo->sheet_id}" .
        "/values/{$data['range']}?valueInputOption={$valueInputOption}");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
curl_setopt($ch, CURLOPT_POSTFIELDS, $dataJSON);
$result = curl_exec($ch);
echo $result, "\n";
curl_close($ch);

function base64url_encode($data) { 
    return rtrim(strtr(base64_encode($data), '+/', '-_'), '='); 
}
